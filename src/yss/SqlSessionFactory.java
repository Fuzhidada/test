package yss;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public  class SqlSessionFactory {
public static Connection getConnection()  {

    Connection connection= null;
    try {
        Class.forName("com.mysql.jdbc.Driver");
        connection = DriverManager
                .getConnection("jdbc:mysql://localhost:3306/fuzhi","root","");
    } catch (SQLException e) {
        e.printStackTrace();
    } catch (ClassNotFoundException e) {
        e.printStackTrace();
    }
    return connection;
}
}
