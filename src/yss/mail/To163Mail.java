package yss.mail;

import javax.activation.DataHandler;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.*;
import java.util.Properties;

public class To163Mail {
    public boolean sedn_163mail(String Fuser, String Fpassword, String Tuser, String mailTitle, String mailConent, String filePath) {
        boolean send_result = false;
        try {
            Properties props = new Properties();
            props.put("mail.smtp.auth", "true");         // 发送服务器需要身份验证 处理反垃圾机制
            props.put("mail.smtp.host", "smtp.163.com"); //发件人的服务服务

            props.put("mail.user", Fuser);
            props.put("mail.password", Fpassword);

            // 构建授权信息，用于进行SMTP进行身份验证
            Authenticator authenticator = new Authenticator() {
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    String username = props.getProperty("mail.user");
                    String password = props.getProperty("mail.password");
                    return new PasswordAuthentication(username, password);
                }
            };

            // 使用环境属性和授权信息，创建邮件会话
            Session mailSession = Session.getInstance(props, authenticator);
            // 创建邮件消息
            MimeMessage message = new MimeMessage(mailSession);
            // 设置发件人
            String username = props.getProperty("mail.user");
            InternetAddress form = new InternetAddress(username);
            message.setFrom(form);

            // 设置收件人   设置多个就为批量发送
            InternetAddress tos = new InternetAddress(Tuser);
            message.setRecipient(MimeMessage.RecipientType.BCC, tos);
//            message.setRecipient(MimeMessage.RecipientType.TO,new InternetAddress("1316286513@qq.com"));
            //换成add为添加多个收件人 --   BCC为秘密抄送  CC为抄送 To为发送
            // 设置邮件标题
            message.setSubject(mailTitle);

            Multipart multipart = new MimeMultipart();
            BodyPart bodyPart = new MimeBodyPart();
            ((MimeBodyPart) bodyPart).setText(mailConent, "UTF-8");

            if (filePath != null) {
                FileDataSource dataSource = new FileDataSource(filePath);
                bodyPart.setDataHandler(new DataHandler(dataSource));

                bodyPart.setFileName(MimeUtility.encodeWord(dataSource.getFile().getName()));//MimeUtility.encodeWord 防止文件乱码
            }
            multipart.addBodyPart(bodyPart);
            // 设置邮件的内容体
            message.setContent(multipart, "UTF-8");
            // 发送邮件
            Transport.send(message);
            send_result = true;
            return send_result;
        } catch (AddressException e) {
            e.printStackTrace();
            return send_result;
        } catch (MessagingException e) {
            e.printStackTrace();
            return send_result;
        } catch (Exception e) {
            e.printStackTrace();
            return send_result;
        }
    }
}
