package yss.excel;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import yss.excel.yss.postman.SendUrl;
import yss.excelbeans.TInterfaces;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;

import static jdk.nashorn.internal.runtime.regexp.joni.Config.log;

public class WriteExcel {

    public int WriteExcel(String path, int sheetat) {
        try {
            SendUrl sendUrl = new SendUrl();

            File excel = new File(path);
            InputStream guoShou = new FileInputStream(excel);
            Workbook workbook = new XSSFWorkbook(guoShou);
            XSSFSheet sheet = ((XSSFWorkbook) workbook).getSheetAt(sheetat);

            XSSFCellStyle xssfCellStyle = ((XSSFWorkbook) workbook).createCellStyle();
            XSSFFont font = ((XSSFWorkbook) workbook).createFont();
            font.setColor(IndexedColors.SEA_GREEN.getIndex());
            font.setFontHeightInPoints((short) 16);
            font.setBold(true);
            xssfCellStyle.setFont(font);

            XSSFCellStyle xssfCellStyleF = ((XSSFWorkbook) workbook).createCellStyle();
            XSSFFont redfont = ((XSSFWorkbook) workbook).createFont();
            redfont.setColor(IndexedColors.RED.getIndex());
            redfont.setFontHeightInPoints((short) 16);
            xssfCellStyleF.setFont(redfont);

            XSSFCellStyle xssfCellStyle1 = ((XSSFWorkbook) workbook).createCellStyle();
            XSSFFont bluefont = ((XSSFWorkbook) workbook).createFont();
            bluefont.setColor(IndexedColors.BLUE_GREY.getIndex());
            bluefont.setFontHeightInPoints((short) 16);
            bluefont.setBold(true);
            xssfCellStyle1.setFont(bluefont);
            xssfCellStyle1.setWrapText(true);

            XSSFCellStyle xssfCellStylex = ((XSSFWorkbook) workbook).createCellStyle();
            xssfCellStylex.setWrapText(true);//自动换行
            xssfCellStylex.setFont(font);

            int lastRow = sheet.getLastRowNum();

            /*
             * @deprecated 读取每一行的url 封装成bean 传递到senduRL
             *
             */

            for (int i = 4; i <= lastRow; i++) {
                TInterfaces tInterfaces = new TInterfaces();
                tInterfaces.setID(sheet.getRow(i).getCell(0));
                tInterfaces.setDescribaction(sheet.getRow(i).getCell(1));
                tInterfaces.setRequirement(sheet.getRow(i).getCell(2));
                tInterfaces.setUrl(sheet.getRow(i).getCell(3));
                tInterfaces.setMode(sheet.getRow(i).getCell(4));
                tInterfaces.setParameter(sheet.getRow(i).getCell(5));
                tInterfaces.setAspectResult(sheet.getRow(i).getCell(7));
                String result = sendUrl.sendUrl(tInterfaces); //获取返回值

                log.println(result);

                sheet.getRow(i).setHeight((short) 1000);
                Cell r6 = sheet.getRow(i).createCell(6);

                try {
                    r6.setCellValue(result);
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                    r6.setCellValue("data too lang");
                }
                r6.setCellStyle(xssfCellStylex);

                Cell r8 = sheet.getRow(i).createCell(8);

                try {

                    if ("[]".equals(result) || "".equals(result)) {
                        r6.setCellValue("无返回结果或为 '[]'");
                        r6.setCellStyle(xssfCellStyle1);
                        if (sheet.getRow(i).getCell(7).toString().equals(tInterfaces.getAspectResult().toString())) {
                            r8.setCellValue("P");
                            r8.setCellStyle(xssfCellStyle);
                        } else {
                            r8.setCellValue("F");
                            r8.setCellStyle(xssfCellStyleF);
                        }

                    } else {
                        if (result.substring(0, 5).equals("FAIL:")) { // 失败消息
                            r8.setCellValue("F"); //直接请求失败
                            r8.setCellStyle(xssfCellStyleF);
                            r6.setCellStyle(xssfCellStyle1);
                        } else {
                            if (sheet.getRow(i).getCell(6).toString().equals(tInterfaces.getAspectResult().toString())) {
                                r8.setCellValue("P");
                                r8.setCellStyle(xssfCellStyle);
                            } else {
                                r8.setCellValue("F");
                                r8.setCellStyle(xssfCellStyleF);
                                r6.setCellStyle(xssfCellStyle1);
                            }

                        }

                    }
                    log.println("第" + i + "列" + tInterfaces);
                    log.println("---------------end---------------------");
                } catch (NullPointerException x) {
                    log.println("链接不完全");
                } catch (StringIndexOutOfBoundsException x) {
                    log.println("无返回值");
                }
            }
            FileOutputStream out = new FileOutputStream(path);
            workbook.write(out);
            out.flush();
            out.close();
            guoShou.close();
            workbook.close();
            log.println("接口测试完成");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return 1;
    }
}
