package yss.excel.yss.postman;

import sun.net.www.protocol.http.Handler;
import yss.excelbeans.TInterfaces;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;


public class SendUrl {
    public String sendUrl(TInterfaces ti) throws IOException {
        URL url = null;
        URLConnection connection = null;
        InputStream os = null;
        InputStream error = null;
        BufferedReader in = null;
        OutputStream outtoServer = null;
        String result = "";
        try {          //excel中为空的不能toString
            if (ti.getMode().toString().equals("get")) {
                if (ti.getParameter() == null) {
                    url = new URL(null, ti.getUrl().toString(), new Handler());
                } else {
                    url = new URL(null, ti.getUrl().toString() + ti.getParameter(), new Handler());
                }
                connection = url.openConnection();

            } else {
                if (ti.getMode().toString().equals("post")) {
                    url = new URL(null, ti.getUrl().toString(), new Handler());
                    connection = url.openConnection();
                    connection.setRequestProperty("Request Method", "POST");
                    connection.setDoOutput(true);
                    connection.setDoInput(true);
                    outtoServer = connection.getOutputStream();
                    if (ti.getParameter() != null) {
                        outtoServer.write(ti.getParameter().toString().getBytes());
                    }
                }
            }

            os = connection.getInputStream();

            in = new BufferedReader(new InputStreamReader(os));
            String line = null;
            while ((line = in.readLine()) != null) {
                result += line;
            }
            return result;

        } catch (Exception e) {
            try {
                error = ((HttpURLConnection) connection).getErrorStream();
                if (error != null) {

                    in = new BufferedReader(new InputStreamReader(error));
                    String linex = null;
                    while ((linex = in.readLine()) != null) {
                        result += linex;
                    }
                    String[] a = result.split("There was an unexpected error");
                    return "FAIL:" + a[1];
                } else {
                    return e + "服务未开启或链接错误";
                }
            } catch (Exception xe) {
                return e + "服务未开启或链接错误";
            }
        }
    }
}


