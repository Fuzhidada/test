package yss.excelbeans;

import org.apache.poi.xssf.usermodel.XSSFCell;

import java.util.Objects;

public class TInterfaces {
    private XSSFCell ID;                //用例id
    private XSSFCell describaction;  //用例描述
    private XSSFCell requirement;     //前置条件
    private XSSFCell url;           //请求url
    private XSSFCell mode;         //请求方式
    private XSSFCell parameter;       //参数
    private XSSFCell resultData;        //返回数据
    private XSSFCell aspectResult;        //预期结果
    private XSSFCell finallyRelult;       //测试结果

    public TInterfaces() {
    }

    public TInterfaces(XSSFCell ID, XSSFCell describaction, XSSFCell requirement, XSSFCell url, XSSFCell mode, XSSFCell parameter, XSSFCell resultData, XSSFCell aspectResult, XSSFCell finallyRelult) {
        this.ID = ID;
        this.describaction = describaction;
        this.requirement = requirement;
        this.url = url;
        this.mode = mode;
        this.parameter = parameter;
        this.resultData = resultData;
        this.aspectResult = aspectResult;
        this.finallyRelult = finallyRelult;
    }

    public XSSFCell getID() {
        return ID;
    }

    public void setID(XSSFCell ID) {
        this.ID = ID;
    }

    public XSSFCell getDescribaction() {
        return describaction;
    }

    public void setDescribaction(XSSFCell describaction) {
        this.describaction = describaction;
    }

    public XSSFCell getRequirement() {
        return requirement;
    }

    public void setRequirement(XSSFCell requirement) {
        this.requirement = requirement;
    }

    public XSSFCell getUrl() {
        return url;
    }

    public void setUrl(XSSFCell url) {
        this.url = url;
    }

    public XSSFCell getMode() {
        return mode;
    }

    public void setMode(XSSFCell mode) {
        this.mode = mode;
    }

    public XSSFCell getParameter() {
        return parameter;
    }

    public void setParameter(XSSFCell parameter) {
        this.parameter = parameter;
    }

    public XSSFCell getResultData() {
        return resultData;
    }

    public void setResultData(XSSFCell resultData) {
        this.resultData = resultData;
    }

    public XSSFCell getAspectResult() {
        return aspectResult;
    }

    public void setAspectResult(XSSFCell aspectResult) {
        this.aspectResult = aspectResult;
    }

    public XSSFCell getFinallyRelult() {
        return finallyRelult;
    }

    public void setFinallyRelult(XSSFCell finallyRelult) {
        this.finallyRelult = finallyRelult;
    }

    @Override
    public String toString() {
        return "TInterfaces{" +
                "ID=" + ID +
                ", describaction=" + describaction +
                ", requirement=" + requirement +
                ", url=" + url +
                ", mode=" + mode +
                ", parameter=" + parameter +
                ", resultData=" + resultData +
                ", aspectResult=" + aspectResult +
                ", finallyRelult=" + finallyRelult +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TInterfaces that = (TInterfaces) o;
        return Objects.equals(ID, that.ID) &&
                Objects.equals(describaction, that.describaction) &&
                Objects.equals(requirement, that.requirement) &&
                Objects.equals(url, that.url) &&
                Objects.equals(mode, that.mode) &&
                Objects.equals(parameter, that.parameter) &&
                Objects.equals(resultData, that.resultData) &&
                Objects.equals(aspectResult, that.aspectResult) &&
                Objects.equals(finallyRelult, that.finallyRelult);
    }

    @Override
    public int hashCode() {

        return Objects.hash(ID, describaction, requirement, url, mode, parameter, resultData, aspectResult, finallyRelult);
    }
}
