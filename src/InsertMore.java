import yss.SqlSessionFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author fuzhi
 * @describte 批量插入一千万条数据
 */
public class InsertMore extends SqlSessionFactory {
    public static void main(String[] args) {
        Integer control = 100;
//        new InsertMore().insert1(control);
        new InsertMore().select();
    }


    private  void select(){
        Connection connection = null;
        PreparedStatement a,b = null;
        ResultSet rs1,rs2=null;
        try {
            connection = getConnection();
            a=connection.prepareStatement("select * from test ");
            b=connection.prepareStatement("select * from test1");

            rs1=a.executeQuery();
            rs2=a.executeQuery();
            List<a> l1=new ArrayList();
            List<a> l2=new ArrayList();
           a as=new a();

            while (rs1.next()){
                as.setA1(rs1.getInt(1));
                as.setA2(rs1.getInt(2));
                as.setA3(rs1.getInt(3));
                as.setA4(rs1.getInt(4));
                l1.add(as);
            }

            while (rs2.next()){
            as.setA1(rs2.getInt(1));
                as.setA2(rs2.getInt(2));
                as.setA3(rs2.getInt(3));
                as.setA4(rs2.getInt(4));
                l2.add(as);
            }
            Long time1 = System.currentTimeMillis();
            for(a g:l2){
                for(a g2:l1){
              if(g.a1==g2.a1){
    System.out.println(g.a1);
                    }
                 }
            }
            Long time2 = System.currentTimeMillis();
            System.out.println("总用时:" + (time2 - time1));
        }catch (Exception e){
e.printStackTrace();
        }


    }

    private void insert1(Integer control) {
        Connection connection = null;
        PreparedStatement preparedStatement = null;
        int set;
        int sum = 0;
        String dealString;
        Long time1 = System.currentTimeMillis();
        try {
            connection = getConnection();
            StringBuilder stringBuilder = new StringBuilder("insert into test1 values");
            int k = 0;
            int k1 = 1000;
            for (int j = 0; j < control; j++) {
                for (; k < k1; k++) {
                    stringBuilder.append("(").append(k).append(",").append(k).append(",").append(k).append(",").append(k).append("),");
                }
                dealString = stringBuilder.substring(0, stringBuilder.length() - 1);

                System.out.println(dealString);
                preparedStatement = connection.prepareStatement(dealString);
                set = preparedStatement.executeUpdate();
                stringBuilder = new StringBuilder("insert into test1 values");
                k1 += 1000;
                sum += set;
            }

            System.out.println("总记录:" + sum);
            Long time2 = System.currentTimeMillis();
            System.out.println("总用时:" + (time2 - time1));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null) {
                    preparedStatement.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    static class a{
        int a1;
        int a2;
        int a3;
        int a4;

        public int getA1() {
            return a1;
        }

        public void setA1(int a1) {
            this.a1 = a1;
        }

        public int getA2() {
            return a2;
        }

        public void setA2(int a2) {
            this.a2 = a2;
        }

        public int getA3() {
            return a3;
        }

        public void setA3(int a3) {
            this.a3 = a3;
        }

        public int getA4() {
            return a4;
        }

        public void setA4(int a4) {
            this.a4 = a4;
        }
    }

}
